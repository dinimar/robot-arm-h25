package ru.kpfu.lirs;

import ru.kpfu.lirs.math.matrix.Matrix3D;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class App {
	public static double BASE_ROT_COEF = 3.2;
	public static double LIFT_ROT_COEF = 4.95;
	public static double REV_ROT_COEF = 1;
	public static RegulatedMotor baseMtr = new EV3LargeRegulatedMotor(MotorPort.C);
	public static RegulatedMotor liftMtr = new EV3LargeRegulatedMotor(MotorPort.B);
	public static RegulatedMotor revMtr = new EV3MediumRegulatedMotor(MotorPort.A);
	
	public static void main(String[] args) throws Exception{
		LCD.drawString("Program is started.", 0, 0);
		LCD.drawString("Manipulator is moving.", 0, 1);
		
//		Set speed to all motors
		revMtr.setSpeed(20);
		baseMtr.setSpeed(20);
		liftMtr.setSpeed(20);
		
//		Input angles
		int baseMtrAngle = -25;
		int liftMtrAngle = -25;
		int revMtrAngle = -25;
		
		// Calculate start position
		Matrix3D t0 = new Matrix3D();
//		Base joint coordinate
		Matrix3D t01 = new Matrix3D();
		Matrix3D t12 = new Matrix3D();
		t12.rotateZ(baseMtrAngle);
		t12.translate(0, -0.07, 0.12);
		
		
//		Lift joint coordinate		
		Matrix3D t23 = new Matrix3D();
		t23.rotateX(liftMtrAngle);
		t23.translate(0, 0.19, -0.06);
		

//		Revolute joint
		Matrix3D t34 = new Matrix3D();
		t34.rotateY(revMtrAngle);
		t34.translate(0, 0, -0.05);
		

//		Rotate joints
		rotateBaseMotor(baseMtrAngle);
		rotateLiftMotor(liftMtrAngle);
		rotateRevMotor(-revMtrAngle);
		
//		Calculate position of EE
		t0.postMultiply(t01);
		t0.postMultiply(t12);
		t0.postMultiply(t23);
		t0.postMultiply(t34);
	
		
		LCD.drawString("EE coordinate:", 0, 2);
//		LCD.drawString(t0.toString(), 0, 3);
		LCD.drawString("X: "+t0.getXasString(), 0, 3);
		LCD.drawString("Y: "+t0.getYasString(), 0, 4);
		LCD.drawString("Z: "+t0.getZasString(), 0, 5);
		
		Delay.msDelay(5000);
	
//		Return to start position 
		rotateRevMotor(0);
		rotateLiftMotor(0);
		rotateBaseMotor(0);
		
		LCD.drawString("Program has finished", 0, 6);
		Delay.msDelay(5000);
	}
	
	public static void rotateBaseMotor(int angle) {
		baseMtr.rotateTo((int)(angle*BASE_ROT_COEF));
	}
	
	public static void rotateLiftMotor(int angle) {
		liftMtr.rotateTo((int)(angle*LIFT_ROT_COEF));
	}
	
	public static void rotateRevMotor(int angle) {
		revMtr.rotateTo((int)(angle*REV_ROT_COEF));
	}
}

