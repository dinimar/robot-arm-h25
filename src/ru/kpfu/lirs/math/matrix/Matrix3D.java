package ru.kpfu.lirs.math.matrix;

public class Matrix3D {
    public double[][] cells = new double[4][4];

    public Matrix3D() { // create a new identity transformation
        cells[0][0] = 1;
        cells[1][1] = 1;
        cells[2][2] = 1;
        cells[3][3] = 1;
    }

    public void rotateX(double theta) { // rotate transformation about the X axis
        Matrix3D tmp = new Matrix3D();

        tmp.cells[1][1] = Math.cos(theta);
        tmp.cells[1][2] = -Math.sin(theta);
        tmp.cells[2][1] = Math.sin(theta);
        tmp.cells[2][2] = Math.cos(theta);

        this.postMultiply(tmp);
    }

    public void rotateY(double theta) { // rotate transformation about the Y axis
        Matrix3D tmp = new Matrix3D();

        tmp.cells[0][0] = Math.cos(theta);
        tmp.cells[0][2] = Math.sin(theta);
        tmp.cells[2][0] = -Math.sin(theta);
        tmp.cells[2][2] = Math.cos(theta);

        this.postMultiply(tmp);
    }

    public void rotateZ(double theta) { // rotate transformation about the Z axis
        Matrix3D tmp = new Matrix3D();

        tmp.cells[0][0] = Math.cos(theta);
        tmp.cells[0][1] = -Math.sin(theta);
        tmp.cells[1][0] = Math.sin(theta);
        tmp.cells[1][1] = Math.cos(theta);

        this.postMultiply(tmp);
    }

    public void translate(double a, double b, double c) { // translate
        cells[0][3] = a;
        cells[1][3] = b;
        cells[2][3] = c;
    }

    public void postMultiply(Matrix3D matrix) {
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                double[] col = new double[]{matrix.cells[0][j], matrix.cells[1][j], matrix.cells[2][j], matrix.cells[3][j]};
                cells[i][j] = rowColMultiply(cells[i], col);
            }
        }
    }

    public double rowColMultiply(double[] row, double[] col) {
        double newCell = 0;

        for(int i=0; i < row.length; i++) {
            newCell += row[i]*col[i];
        }

        return newCell;
    }

    public String getXasString(){
    	return String.valueOf(cells[0][3]);
    }
    
    public String getYasString(){
    	return String.valueOf(cells[1][3]);
    }
    
    public String getZasString(){
    	return String.valueOf(cells[2][3]);
    }
    
    public String toString() {
    	return String.valueOf(cells[0][3]).substring(0, 5)+";"
                +String.valueOf(cells[1][3]).substring(0, 5)+";"
                +String.valueOf(cells[2][3]).substring(0, 4);   
	}
}